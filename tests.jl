include("main.jl")

using Test

function test_checkWill() 
    @test !checkWill([0, 1])
    @test checkWill([1, 4, 6, 7])
    @test checkWill([0, 1, 2, 3, 4, 5, 6, 7, 8, 9,])
    println("All the tests of checkWill worked out.") 
end

function test_checkTaki() 
    @test !checkTaki([0, 1])
    @test !checkTaki([1, 4, 6, 7])
    @test !checkTaki([1, 1, 7, 7])
    @test checkTaki([1, 1, 1, 4, 6, 7, 7, 7])
    println("The function checkTaki is working.")
end

function test_checkJackson()
    @test !checkJackson(10, [0, 1])
    @test !checkJackson(42, [1, 4, 6, 7, 1, 1, 7, 7])
    @test !checkJackson(1000, [1, 1, 7, 7, 1, 0, 0, 0])
    @test checkJackson(1234, [6, 1, 7, 4, 7, 7, 1, 1, 1, 2, 3, 4])
    println("The function checkJackson is working.")
end

function test_checkWillBase()
    @test !checkWillBase(2, [6, 1, 7, 4])
    @test !checkWillBase(6, [6, 7, 8, 9, 6, 7, 8, 9])
    @test checkWillBase(10, [0, 1, 2,3, 4, 5, 6, 7, 8, 9])
    @test checkWillBase(2, [1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0]) 
    @test checkWillBase(8, [1, 4, 0, 3, 6, 9, 0])
    println("The function checkWillBase is working.")
end

function test_convertToTable()
    @test convertToTable(2, 8) ==    [3, 1, 0, 0, 0, 0, 0, 0, 0, 0] 
    @test convertToTable(10, 123) == [0, 1, 1, 1, 0, 0, 0, 0, 0, 0]
    @test convertToTable(1, 8) ==    [8, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    @test convertToTable(3, 9) ==    [2, 1, 0, 0, 0, 0, 0, 0, 0, 0]
    println("The function convertToTable is working.")
end

function test_checkFriends()
    @test checkFriends([2], [2]) == nothing
    @test checkFriends([8, 100], [6, 1, 7, 4]) == nothing
    @test checkFriends([42, 333], [3, 3, 3, 4, 2]) == nothing
    @test checkFriends([24, 42, 6174], [2, 4, 4, 2, 6, 1, 7, 4, 6, 1, 7, 4]) == []
    @test checkFriends([123, 123, 123], [1, 2, 3, 1, 2, 3, 1, 2, 3, 6, 1, 7, 4, 8, 8, 8]) == [8, 8, 8]
    @test checkFriends([], [6, 1, 7, 4, 3, 2, 1]) == [1, 2, 3]
    println("The function checkFriends is working.")
end

test_checkWill()
test_checkTaki()
test_checkJackson()
test_convertToTable()
test_checkWillBase()
test_checkFriends()