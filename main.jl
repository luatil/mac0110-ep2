# 6174
function checkWill(cards)
    has_one = false
    has_six = false
    has_seven = false
    has_four = false
    for el in cards 
        if el == 6
            has_six = true
        elseif el == 1
            has_one = true
        elseif el == 7
            has_seven = true
        elseif el == 4
            has_four = true
        end
    end
    if has_one && has_six && has_seven && has_four 
        return true
    else 
        return false
    end 
end

# 6174 && 7711
function checkTaki(cards)
    compare = [0, 3, 0, 0, 1, 0, 1, 3, 0, 0] 
    temp = fill(0, 10)
    for i in cards
        temp[i+1] += 1
    end
    for j in 1:10
        if temp[j] < compare[j]
            return false
        end
    end
    return true
end

# 6174 && 7711 && x
function checkJackson(x, cards)
    compare = [0, 3, 0, 0, 1, 0, 1, 3, 0, 0] 
    temp = fill(0, 10)
    while x > 0
        aux = x % 10
        compare[aux+1] += 1
        x = div(x, 10)
    end
    for i in cards
        temp[i+1] += 1
    end
    for j in 1:10
        if temp[j] < compare[j]
            return false
        end
    end
    return true
end

function convertToTable(base, number)
    arr = fill(0, 10)
    if base == 1
        arr[1] = number
        return arr
    end
    while number > 0
        temp = number % base
        arr[temp+1] += 1
        number = div(number, base)
    end
    return arr
end

function checkWillBase(b, cards)
    to_compare = convertToTable(b, 6174)
    temp = fill(0, 10)
    for el in cards
        temp[el+1] += 1
    end
    for i in 1:b
        if temp[i] < to_compare[i]
            return false
        end
    end
    return true
end

# 6174 && numbers 
function checkFriends(numbers, cards)
    arr = [0, 1, 0, 0, 1, 0, 1, 1, 0, 0]
    for el in numbers
        while el > 0
            j = el % 10
            arr[j+1] += 1
            el = div(el, 10)
        end
    end
    for i in cards
        arr[i+1] -= 1
    end
    ans = []
    for i in 1:10
        if arr[i] > 0
            return nothing
        elseif arr[i] < 0
            while arr[i] < 0
                push!(ans, i-1) 
                arr[i] += 1
            end
        end
    end
    return ans
end
